package com.n0r0eq1.mapboxtask.di

import android.content.Context
import android.content.SharedPreferences
import androidx.room.Room
import com.n0r0eq1.mapboxtask.data.db.AppDatabase
import com.n0r0eq1.mapboxtask.data.network.NetworkManager
import com.n0r0eq1.mapboxtask.data.network.NetworkManagerImpl
import com.n0r0eq1.mapboxtask.data.repository.cars_repository.CarsRepository
import com.n0r0eq1.mapboxtask.data.repository.cars_repository.CarsRepositoryImpl
import com.n0r0eq1.mapboxtask.data.repository.location_repository.LocationRepository
import com.n0r0eq1.mapboxtask.data.repository.location_repository.LocationRepositoryImpl
import org.koin.android.ext.koin.androidContext
import org.koin.core.module.Module
import org.koin.dsl.module

/**
 * Application-level class for providing koin-modules
 */
object KoinModulesProvider {

    private const val PREFERENCES_FILE_NAME = "SharedPreferences"
    private const val DATABASE_NAME = "MapboxTaskDb"

    /**
     * Get all modules
     */
    fun getList(): List<Module> {
        return listOf(
            getAppModule(),
            getNetworkModule(),
            getDbModule(),
            getRepoModule(),
            getPresentationModule()
        )
    }

    private fun getAppModule(): Module {
        return module {
            single<SharedPreferences> {
                androidContext().getSharedPreferences(PREFERENCES_FILE_NAME, Context.MODE_PRIVATE)
            }
        }
    }

    private fun getNetworkModule(): Module {
        return module {
            single<NetworkManager> { NetworkManagerImpl() }
        }
    }

    private fun getDbModule(): Module {
        return module {
            single(createdAtStart=true) {
                Room.databaseBuilder(androidContext(), AppDatabase::class.java, DATABASE_NAME)
                    .build()
            }
        }
    }

    private fun getRepoModule(): Module {
        return module {
            single<CarsRepository> { CarsRepositoryImpl(get(), get()) }
            single<LocationRepository> { LocationRepositoryImpl() }
        }
    }

    private fun getPresentationModule(): Module {
        return module {
            //
        }
    }
}