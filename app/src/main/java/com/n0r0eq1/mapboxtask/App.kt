package com.n0r0eq1.mapboxtask

import android.app.Application
import com.n0r0eq1.mapboxtask.di.KoinModulesProvider
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

/**
 * Application class
 */
class App : Application() {

    override fun onCreate() {
        super.onCreate()
        initDi()
    }

    /**
     * DI initialization
     */
    private fun initDi() {
        startKoin {
            androidContext(this@App)
            modules(KoinModulesProvider.getList())
        }
    }
}