package com.n0r0eq1.mapboxtask.presentation.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.n0r0eq1.mapboxtask.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}