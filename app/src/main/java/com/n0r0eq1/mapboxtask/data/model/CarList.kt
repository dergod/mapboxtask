package com.n0r0eq1.mapboxtask.data.model

/**
 * Model with all cars data
 */
data class CarList(
    val cars: List<Car>
)