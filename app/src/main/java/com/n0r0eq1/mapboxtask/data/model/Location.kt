package com.n0r0eq1.mapboxtask.data.model

import androidx.room.ColumnInfo

/**
 * Model with location data
 * (user location, car location...)
 */
data class Location(
    @ColumnInfo(name = "latitude") val latitude: Double?,
    @ColumnInfo(name = "longitude") val longitude: Double?
)