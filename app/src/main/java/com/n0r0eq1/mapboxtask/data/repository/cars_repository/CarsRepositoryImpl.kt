package com.n0r0eq1.mapboxtask.data.repository.cars_repository

import com.n0r0eq1.mapboxtask.data.db.AppDatabase
import com.n0r0eq1.mapboxtask.data.model.CarList
import com.n0r0eq1.mapboxtask.data.model.DataResult
import com.n0r0eq1.mapboxtask.data.network.NetworkManager

/**
 * Repository for working with fleet of cars
 * (implementation)
 */
class CarsRepositoryImpl(
        private val networkManager: NetworkManager,
        private val database: AppDatabase
) : CarsRepository {

    /**
     * Method for getting all cars from server
     */
    private suspend fun getCarsFromNetwork(): DataResult<CarList> {
        return networkManager.getCars()
    }

    /**
     * Method for getting all cars from database
     */
    private suspend fun getCarsFromDb(): DataResult<CarList> {
        val cars = CarList(database.carDao().getAll())
        return DataResult.Success(cars)
    }

    /**
     * Method for saving all cars to database
     */
    private suspend fun saveCarsToDb(carList: CarList) {
        database.carDao().insertAll(*carList.cars.map { it }.toTypedArray())
    }
}