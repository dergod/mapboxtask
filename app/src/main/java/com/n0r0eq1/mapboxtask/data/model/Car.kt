package com.n0r0eq1.mapboxtask.data.model

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Model with car data
 */
@Entity
data class Car(
    @PrimaryKey val uid: Int,
    @ColumnInfo(name = "name") val name: String?,
    @ColumnInfo(name = "plate_number") val plateNumber: String?,
    @ColumnInfo(name = "color") val color: String?,
    @ColumnInfo(name = "angle") val angle: Int?,
    @ColumnInfo(name = "fuel_percentage") val fuelPercentage: Int?,
    @Embedded val location: Location?
)