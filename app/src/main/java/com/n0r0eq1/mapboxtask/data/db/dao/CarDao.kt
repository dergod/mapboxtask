package com.n0r0eq1.mapboxtask.data.db.dao

import androidx.room.*
import com.n0r0eq1.mapboxtask.data.model.Car

/**
 * Data access object for cars
 */
@Dao
interface CarDao {

    @Query("SELECT * FROM car")
    suspend fun getAll(): List<Car>

    @Query("SELECT * FROM car WHERE uid IN (:userIds)")
    suspend fun loadAllByIds(userIds: IntArray): List<Car>

    @Query("SELECT * FROM car WHERE name LIKE :name LIMIT 1")
    suspend fun findByName(name: String): Car

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg users: Car)

    @Delete
    suspend fun delete(user: Car)
}