package com.n0r0eq1.mapboxtask.data.network

import com.n0r0eq1.mapboxtask.BuildConfig
import com.n0r0eq1.mapboxtask.data.model.CarList
import com.n0r0eq1.mapboxtask.data.model.DataResult
import io.ktor.client.*
import io.ktor.client.engine.android.*
import io.ktor.client.features.json.*
import io.ktor.client.features.logging.*
import io.ktor.client.request.*

/**
 * Class for network operations
 */
class NetworkManagerImpl : NetworkManager {

    companion object {
        private const val BASE_URL = "https://raw.githubusercontent.com/Gary111/TrashCan/master"
        private const val GET_CARS_URL = "$BASE_URL/2000_cars.json"
    }

    /**
     * Init
     */
    private val client = HttpClient(Android) {
        install(JsonFeature) {
            serializer = GsonSerializer()
        }
        if (BuildConfig.DEBUG) {
            install(Logging) {
                logger = Logger.DEFAULT
                level = LogLevel.ALL
            }
        }
        // BadResponseStatus - if http error code < 300
        // (serialization error without this line)
        expectSuccess = true
    }

    /**
     * Method for getting all cars data
     */
    override suspend fun getCars(): DataResult<CarList> {
        return try {
            DataResult.Success(client.get(GET_CARS_URL))
        } catch (cause: Throwable) {
            DataResult.Error(cause.message ?: "")
        }
    }

    /**
     * To complete the work correctly
     */
    override fun onDestroy() {
        client.close()
    }
}