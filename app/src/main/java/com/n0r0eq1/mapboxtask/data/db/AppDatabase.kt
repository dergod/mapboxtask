package com.n0r0eq1.mapboxtask.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.n0r0eq1.mapboxtask.data.db.dao.CarDao
import com.n0r0eq1.mapboxtask.data.model.Car

/**
 * Application level database singleton (via DI)
 */
@Database(entities = [Car::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    /**
     * For working with car data
     */
    abstract fun carDao(): CarDao
}