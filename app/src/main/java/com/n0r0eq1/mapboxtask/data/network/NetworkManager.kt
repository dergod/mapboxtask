package com.n0r0eq1.mapboxtask.data.network

import com.n0r0eq1.mapboxtask.data.model.CarList
import com.n0r0eq1.mapboxtask.data.model.DataResult

/**
 * Interface for network operations
 */
interface NetworkManager {

    /**
     * Method for getting all cars data
     */
    suspend fun getCars(): DataResult<CarList>

    /**
     * To complete the work correctly
     */
    fun onDestroy() // todo - add call on Activity onDestroy
}